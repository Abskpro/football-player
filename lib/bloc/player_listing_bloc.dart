import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_player/services/repository.dart';
import '../models/api_model.dart';
import 'player_listing_state.dart';
import 'package:rxdart/rxdart.dart';
import 'player_listing_events.dart';

class PlayerListingBloc extends Bloc<PlayerListingEvent, PlayerListingState> {
  final PlayerRepository playerRepository;

  PlayerListingBloc({this.playerRepository})
      : super(PlayerUninitializedState());

  @override
  Stream<Transition<PlayerListingEvent, PlayerListingState>> transformEvents(
      Stream<PlayerListingEvent> events, transitionFn) {
    return events
        .debounceTime(const Duration(milliseconds: 300))
        .switchMap((transitionFn));
  }

  @override
  void onTransition(
      Transition<PlayerListingEvent, PlayerListingState> transition) {
    super.onTransition(transition);
    print(transition);
  }

  @override
  Stream<PlayerListingState> mapEventToState(PlayerListingEvent event) async* {
    yield PlayerFetchingState();
    List<Players> players;
    try {
      if (event is CountrySelectedEvent) {
        players = await playerRepository
            .fetchPlayersByCountry(event.nationModel.countryId);
      } else if (event is SearchTextChangedEvent) {
        print("hitting service");
        players = await playerRepository.fetchPlayersByName(event.searchTerm);
      } else if (event is AdvanceSearchChangeEvent) {
        players = await playerRepository
            .fetchPlayersSearchConfiguration(event.searchConfiguration);
      }

      if (players.isEmpty) {
        yield PlayerEmptyState();
      } else {
        yield PlayerFetchedState(players: players);
      }
    } catch (_) {
      yield PlayerErrorState();
    }
  }
}
