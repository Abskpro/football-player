import 'package:bloc_player/bloc/player_listing_bloc.dart';
import 'package:bloc_player/bloc/player_listing_state.dart';
import 'package:bloc_player/models/api_model.dart';
import 'package:bloc_player/themes/themes.dart';
import 'package:bloc_player/widgets/message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PlayerListing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PlayerListingBloc, PlayerListingState>(
      builder: (context, state) {
        print(state);
        if (state is PlayerUninitializedState) {
          return Message(
              message: "Please select a country flag to fetch players from");
        } else if (state is PlayerEmptyState) {
          return Message(message: "No Players found");
        } else if (state is PlayerErrorState) {
          return Message(message: "Something went wrong");
        } else if (state is PlayerFetchingState) {
          return Expanded(child: Center(child: CircularProgressIndicator()));
        } else {
          final stateAsPlayerFetchedState = state as PlayerFetchedState;
          final players = stateAsPlayerFetchedState.players;
          return buildPlayersList(players);
        }
      },
    );
  }

  Widget buildPlayersList(List<Players> players) {
    return Expanded(
      child: ListView.separated(
        itemBuilder: (BuildContext context, index) {
          Players player = players[index];
          return ListTile(
            // leading: Image.network(
            //   player.headshot.imgUrl,
            //   width: 70.0,
            //   height: 70.0,
            // ),
            trailing: Text(
              player.rating.toString() + "  " + player.position,
              style: titleStyle,
            ),
            isThreeLine: true,
            title: Text(player.name, style: titleStyle),
            subtitle: Text(
                player.club.name +
                    " | " +
                    player.league.name +
                    " | " +
                    player.nation.name,
                style: subTitleStyle),
          );
        },
        separatorBuilder: (BuildContext context, index) {
          return Divider(
            height: 8.0,
            color: Colors.transparent,
          );
        },
        itemCount: players.length,
      ),
    );
  }

  // Widget buildPlayersList(List<Players> players) {
  //   return Expanded(
  //     child: ListView.separated(
  //       itemBuilder: (BuildContext context, index) {
  //         Players player = players[index];
  //         return ListTile(
  //           // leading: Image.network(
  //           //   player.headshot.imgUrl,
  //           //   width: 70.0,
  //           //   height: 70.0,
  //           // ),
  //           title: Text(player.name, style: titleStyle),
  //           subtitle: Text(player.club.name, style: subTitleStyle),
  //         );
  //       },
  //       separatorBuilder: (BuildContext context, index) {
  //         return Divider(
  //           height: 8.0,
  //           color: Colors.transparent,
  //         );
  //       },
  //       itemCount: players.length,
  //     ),
  //   );
  // }
}
