import 'package:bloc_player/bloc/player_listing_bloc.dart';
import 'package:bloc_player/bloc/player_listing_events.dart';
import 'package:bloc_player/pages/advanced_search_page.dart';
import 'package:bloc_player/pages/player_listing.dart';
import 'package:bloc_player/search_configuration.dart';
import 'package:bloc_player/services/repository.dart';
import 'package:bloc_player/themes/themes.dart';
import 'package:bloc_player/widgets/horizontal_bar.dart';
import 'package:bloc_player/widgets/search_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  //player provider from the main.dart parent
  final PlayerRepository playerRepository;

  //constructr
  HomePage({this.playerRepository});

  @override
  HomePageState createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  //initializing bloc
  PlayerListingBloc _playerListingBloc;
  SearchConfiguration _searchConfiguration = SearchConfiguration();

  @override
  void initState() {
    super.initState();
    //sets the initial state of the application
    _playerListingBloc =
        PlayerListingBloc(playerRepository: widget.playerRepository);
  }

  /****/
  @override
  void didUpdateWidget(HomePage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    //scan for widget change and update bloc
    return BlocProvider(
      create: (BuildContext context) => _playerListingBloc,
      child: Scaffold(
        floatingActionButton: FloatingActionButton.extended(
          label: Text("Advanced search"),
          icon: Icon(Icons.filter_list),
          onPressed: () async {
            final resultMap = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AdvancedSearchPage(
                          searchConfiguration: _searchConfiguration,
                        )));

            print(resultMap);
            _searchConfiguration = resultMap["search_configuration"];
            print(_searchConfiguration);
            _playerListingBloc.add(AdvanceSearchChangeEvent(
                searchConfiguration: _searchConfiguration));
          },
        ),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
          title: Text(
            'Football Players',
            style: appBarTextStyle,
          ),
        ),
        body: Column(
          children: <Widget>[
            HorizontalBar(),
            SizedBox(height: 10.0),
            SearchBar(),
            SizedBox(height: 10.0),
            PlayerListing()
          ],
        ),
      ),
    );
  }
}
