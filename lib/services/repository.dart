import 'package:bloc_player/models/api_model.dart';
import 'package:bloc_player/search_configuration.dart';
import 'package:bloc_player/services/player_api_provider.dart';

//what is a repository?
class PlayerRepository {
  PlayerApiProvider _playerApiProvider = PlayerApiProvider();

  Future<List<Players>> fetchPlayersByCountry(String countryId) =>
      _playerApiProvider.fetchPlayersByCountry(countryId);

  Future<List<Players>> fetchPlayersByName(String name) =>
      _playerApiProvider.fetchPlayersByName(name);

  Future<List<Players>> fetchPlayersSearchConfiguration(
          SearchConfiguration searchConfiguration) =>
      _playerApiProvider.fetchPlayersSearchConfiguration(searchConfiguration);
}
