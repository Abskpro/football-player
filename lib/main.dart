import 'package:flutter/material.dart';
import 'package:bloc_player/pages/HomePage.dart';
import 'package:bloc_player/services/repository.dart';

void main() {
  //initializing provider
  PlayerRepository _repository = PlayerRepository();

  runApp(MyApp(
    playerRepository: _repository,
  ));
}

class MyApp extends StatelessWidget {
  final PlayerRepository playerRepository;

  MyApp({this.playerRepository});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bloc Implementation',
      theme: ThemeData(
        canvasColor: Colors.white,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            elevation: 0.0,
            textTheme:
                TextTheme(headline1: TextStyle(fontWeight: FontWeight.bold)),
            iconTheme: IconThemeData(color: Colors.black)),
      ),
      home: HomePage(playerRepository: playerRepository),
    );
  }
}
